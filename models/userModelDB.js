const mongoose = require("mongoose");
const Schema = mongoose.Schema;
 
const UserScheme = new Schema(
    {userName: String, userID: Number},
    { versionKey: false });

const User = mongoose.model("User", UserScheme); 
module.exports = User;