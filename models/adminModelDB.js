const mongoose = require("mongoose");
const Schema = mongoose.Schema;
 
const AdminScheme = new Schema(
    {admName: String, admID: Number},
    { versionKey: false });

const Admin = mongoose.model("Admin", AdminScheme); 
module.exports = Admin;
  
