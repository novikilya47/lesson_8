const express = require('express');
const app = express();
const user = require('./routes/user');
const admin = require('./routes/admin');
const fileworker = require('./routes/fileWorker');
const emitter = require('./routes/emitter');
const fs = require("fs");

app.use("/user", user);
app.use("/admin", admin);
app.use("/fileWorker", fileworker);
const mongoose = require("mongoose");

emitter.on('Event', function(fileName){
    let readableStream = fs.createReadStream(fileName.originalname, "utf8");
    let writeableStream = fs.createWriteStream("WriteSimple2.txt");
    readableStream.pipe(writeableStream);
});

mongoose.connect("mongodb://localhost:27017/BD", { useUnifiedTopology: true, useNewUrlParser: true }, function(err, res){
    if(err) {return res.status(500).send('Something broke!');}
    app.listen(3000, function(){
        console.log("Сервер работает");
    });
});


