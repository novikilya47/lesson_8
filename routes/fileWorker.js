const express = require('express');
const fileworker = express.Router();
const fs = require("fs");
const jsonParser = express.json();
const multer = require('multer');
const load = multer();
const emitter = require('./emitter');
const fileworkerController = require("../controllers//fileworkerControllers");



fileworker.post("/writeSimple", jsonParser, fileworkerController.writeSimple);

fileworker.get("/readSimple", fileworkerController.readSimple);

fileworker.post("/writeStream", jsonParser, fileworkerController.writeStream);

fileworker.get("/readStream", fileworkerController.readStream);

fileworker.put("/writeFile", load.single('file'), fileworkerController.readFile);

module.exports = fileworker;