const express = require('express');
const administrator = express.Router();
const jsonParser = express.json();
const adminController = require("../controllers/adminControllers");



administrator.post("/create", jsonParser, adminController.postData);

administrator.get("/:id", adminController.getData);

administrator.delete("/:id", adminController.deleteData);

administrator.put("/:id", jsonParser, adminController.putData);

module.exports = administrator;
