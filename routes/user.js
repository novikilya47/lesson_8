const express = require('express');
const user1 = express.Router();
const urlencodedParser = express.urlencoded({extended: true});
const userController = require("../controllers/userControllers");

global.user = {};

user1.post("/create", urlencodedParser, userController.postData);

user1.get("/:id", userController.getData);

user1.delete("/:id", userController.deleteData);

user1.put("/:id", urlencodedParser, userController.putData);

module.exports = user1;
