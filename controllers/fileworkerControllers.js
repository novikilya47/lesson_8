const fs = require("fs");
const emitter = require('../routes/emitter');

exports.writeSimple = function(request, response){
    console.log(request.body);
    fs.writeFile("writeSimple.txt", JSON.stringify(request.body), 'utf8', function(error){
        if(error){console.log("Ошибка");}
        else {response.send(`<h3>Данные записаны.</h3>`);}
    });
};

exports.readSimple = function(request, response){
    fs.readFile("writeSimple.txt", "utf8", function(error, data){
        if(error){console.log("Ошибка");}
        else {response.send(`<h3>Данные прочитаны. ${data}</h3>`);}
    });
};

exports.writeStream = function (request,response) {
    let writeStream = fs.createWriteStream("writeSimple.txt");
    let data = JSON.stringify(request.body);
    writeStream.write(data);
    response.send(`<h3>Данные записаны.</h3>`);
};

exports.readStream = function (request,response) {
    let readStream = fs.createReadStream("writeSimple.txt", "utf8");
    readStream.on("data", function(chunk){
        response.send(chunk);
    });
};

exports.readFile = function(request,response) {
    let fileName = request.file;    
    emitter.emit('Event', fileName);
    response.send(`<h3>Данные записаны</h3>`);
};

