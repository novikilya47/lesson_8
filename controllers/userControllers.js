const User = require("../models/userModelDB");
 
exports.postData = function(request, response){
    User.create({userName: request.body.userName, userID:request.body.userID}, function(err, doc){     
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные отправлены. ${doc} <h3>`); 
    });      
};

exports.getData = function(request, response){
    User.find({userID:request.params.id}, function(err, doc){           
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.deleteData = function(request, response){
    User.deleteOne({userID:request.params.id}, function (err, result){      
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные удалены </h3>`); 
    });       
};

exports.putData = function(request, response){
    User.updateOne({userID:request.params.id}, {userName: request.body.userName}, function(err, result){
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные обновлены <h3>`);
    });        
};
