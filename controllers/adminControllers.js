const Admin = require("../models/adminModelDB");
 
exports.postData = function(request, response){
    Admin.create({admName: request.body.adminName, admID:request.body.adminId}, function(err, doc){
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные отправлены. ${doc} <h3>`); 
    });      
};

exports.getData = function(request, response){
    Admin.find({admID:request.params.id}, function(err, doc){   
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные получены: ${doc} <h3>`);     
    });      
};

exports.deleteData = function(request, response){
    Admin.deleteOne({admID:request.params.id}, function (err, result){
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные удалены </h3>`); 
    });       
};

exports.putData = function(request, response){
    Admin.updateOne({admID:request.params.id}, {admName: request.body.adminName}, function(err, result){
        if (err) {return response.status(500).send('Something broke!');}
        response.send(`<h3> Данные обновлены <h3>`);
    });        
};
